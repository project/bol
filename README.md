# Bill of Lading

Bill of Lading provides a custom drush command, bol, which generates a list of Drupal structures present on the site, such as:

- blocks
- block types
- content types
- content type fields
- filter formats
- group relationship
- group role
- group type
- image styles
- media types
- paragraphs
- user roles
- views
- vocabularies
- webforms
- workflows

## Features

All of the information in the bill of lading is discoverable at various places in Drupal's admin UI. This module brings the information together in one place, and is particularly useful when doing a site inventory to determine the site's architecture.

The output is available in all supported drush output formats using the --format switch (drush help bol for a list).


## Requirements

Drush version 12 or higher is required.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Using the Module

Enabling the module in the usual way will add a drush command, bol. The command

drush help bol

will provide syntax and option information. The module will analyze the configuration yaml files, so ensure that the site configuration has been exported (drush cex).


## Maintainers

- Jeff Greenberg - [jAyenGreen](https://www.drupal.org/u/j-ayen-green)
