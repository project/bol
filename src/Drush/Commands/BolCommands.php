<?php

namespace Drupal\bol\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Site\Settings;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class BolCommands extends DrushCommands {

  public array $types;

  /**
   * Provide a bill of lading of the site's elements.
   */
  #[CLI\Command(name: 'bol:scan', aliases: ['bol'])]
  #[CLI\Argument(name: 'type', description: 'A single item type to scan: all, block, block-type, content-type, filter-format, group-relationship, group-role, group-type, image-style, media-type, paragraph-type, user-role, view, vocabulary, webform, workflow.')]
  #[CLI\Option(name: 'disabled', description: 'Include disabled items.')]
  #[CLI\Usage(name: 'drush bol:scan', description: 'Provide a bill of lading - output to screen.')]
  #[CLI\Usage(name: 'drush bol:scan --format=csv', description: 'Provide a bill of lading - output to a CSV file.')]
  #[CLI\Usage(name: 'drush bol:scan --disabled', description: 'Provide a bill of lading - output enabled and disabled items.')]
  #[CLI\FieldLabels(labels: [
    'group' => 'Group',
    'name' => 'Name',
    'type' => 'ID',
    'status' => 'Status',
    'description' => 'Description',
  ])]
  #[CLI\DefaultTableFields(fields: ['group', 'name', 'type', 'status', 'description'])]
  #[CLI\FilterDefaultField(field: 'group')]

  public function scan(string $type = 'all', array $options = ['format' => 'table', 'disabled' => FALSE]): RowsOfFields {
    $file = Drupal::service('extension.path.resolver')->getPath('module', 'bol') . '/data/items.yml';
    if (!file_exists($file)) {
      $this->logger()->error(dt("The BOL configuration file, $file, is not present."));
      exit(0);
    }
    $arr = Yaml::decode(file_get_contents($file));
    foreach ($arr['items'] as $types => $data) {
      $this->types[$types] = $data;
    }

    if ($type !== 'all' && !in_array($type, $this->types)) {
      $this->logger()->error(dt("$type is not a valid item type."));
      exit(0);
    }

    $this->logger()->success(dt('Site scanned.'));

    $configPath = Settings::get('config_sync_directory');
    $configPath .= str_ends_with($configPath, '/') ?: '/';

    if ($type == 'all') {
      $useTypes = $this->types;
    }
    else {
      $useTypes = [$type];
    }
    unset($useTypes['field']);

    $all = [];
    foreach ($useTypes as $pathType => $data) {
      $path = $configPath . $this->types[$pathType]['path'];
      $ret = $this->getInfo($path, $pathType, $options);
      if ($ret) {
        $all = array_merge($all, $ret);
      }
    }

    return new RowsOfFields($all);
  }

  /**
   * Convert the associative array of entries into an indexed array.
   */
  private function formatOutput(array $list): array {

    return array_values($list);
  }

  /**
   * Return an array of filenames matching the path.
   */
  private function getGlob(string $path): array {

    return glob($path);
  }

  /**
   * Grab the needed values from each yaml file.
   * @param array $glob
   *   The list of files that match the path that was provided.
   * @param string $pathType
   *   The type of entry the yaml holds (e.g., content type, view).
   * @param array $options
   *   The array of options passed by drush including switches.
   * @return array
   *   An array with each yaml file's values ready for formatting.
   */
  private function decode(array $glob, string $pathType, array $options): array {
    $ret = [];
    if (count($glob) > 0) {
      foreach ($glob as $file) {
        $yaml = Yaml::decode(file_get_contents($file));
        $hold = [];
        if ($options['disabled'] || $yaml['status']) {
          $hold['group'] = $pathType;
          $grid = $this->types[$pathType]['grid'];
          foreach ($grid as $label => $value) {
            $hold[$label] = $yaml[$value] ?? '';
          }
          $hold['status'] = $yaml['status'] ? 'Enabled' : 'Disabled' ?? '';
          $ret[] = $hold;
        }
        if ($pathType == 'content-type') {
          $fields = $this->addFields($yaml['type']);
          if (count($fields) > 0) {
          $ret = array_merge($ret, $fields);
          }
        }
      }
    }

    return $ret;
  }

  /**
   * Obtain a list of config files matching the path.
   * @param string $path
   *   A string containing the path that matches the type of yaml file wanted.
   * @param string $pathType
   *   The type of entry the yaml holds (e.g., content type, view).
   * @param array $options
   *   The array of options passed by drush including switches.
   * @return array
   *   An array with each yaml file's values ready for formatting.
   */
   private function getInfo(string $path, string $pathType, array $options): array {
    $bol = [];
    $glob = $this->getGlob($path);
    $items = $this->decode($glob, $pathType, $options);
    if (count($items) > 0) {
      $bol = $this->formatOutput($items);
    }

    return $bol;
  }

  /**
   * Return field information for the node bundle being evaluated.
   * @param string $bundle
   *
   * @return array
   */
  private function addFields(string $bundle): array {
    $fields = [];
    $hold['group'] = '  field';
    $hold['status'] = 'Enabled';
    $grid = $this->types['field']['grid'];
    $files = $this->getGlob('../config/field.field.node.' . $bundle . '*');
    foreach ($files as $file) {
      $yaml = Yaml::decode(file_get_contents($file));
      foreach ($grid as $label => $value) {
        $hold[$label] = $yaml[$value] ?? '';
      }
      $fields[] = $hold;
    }
    return $fields;
  }

}
